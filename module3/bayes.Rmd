---
title: "R Notebook"
output:
  word_document: default
  html_notebook: default
---

Suppose we fit a curve with basis functions b1(X)=X, b2(X)=(X-1)^2 I(X >= 1). We fit
the linear regression model Y = beta0 + beta1b1(X) + beta2b2(X) + E, and obtain coefficient estimates betacap0 = 1, betacap1=1, betacap2=-2. Sketch the estimated curve between X=-2 and X=2. Note the intercepts, slopes, and other relevant information.

Solution:

```{r}
x = -2:2
y = 1 + x + (-2) * (x-1)^2 * I(x>1)
plot(x, y)
```

As we can see, the curve is linear between -2 and 1 : y = 1+x and quadratic between 1 and 2 : y = 1 + x - 2(x-1)^2.


3. In this problem we will investigate the use of Naive Bayes classifier.

(b) Consider the dataset "South African Heart Disease", used in Homework 3. As in Homework 3, randomly partition the dataset on the training and validation set. [Note: use the same partition of the observations as in Homework 3.] Use the training set, and the continuous predictors only, to classify the variable chd. Evaluate the bandwidth parameter ? of the kernel on a grid, and select the best parameter using crossvalidation. Report the predictive performance on the validation set.


```{r}
library(e1071)
require(caTools)
data <- read.csv("SAheart.data.txt",head=TRUE,sep=",")
data <- data[,-6]
set.seed(101) 
sample = sample.split(data, SplitRatio = 0.5)
trainSet = subset(data, sample == TRUE)
trainSet <- trainSet[,-1]
validSet = subset(data, sample == FALSE)
validSet <- validSet[,-1]
head(trainSet)
```


```{r}
nbmodel <- naiveBayes(chd ~ ., data = trainSet)
nbmodel
```











